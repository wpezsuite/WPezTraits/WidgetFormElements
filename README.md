## WPezTraits: Widget Form Elements

__It's time to do WordPress widgets The ezWay.__

These traits being used in a working example can be found here:  https://gitlab.com/WPezPlugins/wpez-widgets-demo

> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --



### FAQ

**1) Why?**

After looking at other WordPress widget boilerplates, I decided to create something that is more useful and more robust; a "micro-framework" if you will. These traits are one part of that WordPress widgets micro-framework. 

**2) Can I use this in my plugin or theme?**

Yes, but to be safe, please change the namespace. 


**3) Nice work. Can we hire you?**

 Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 




### HELPFUL LINKS

 - https://gitlab.com/WPezPlugins/wpez-widgets-demo/tree/master/App/Core/Traits/WidgetFormElements
 
 - And passed around via this container: https://gitlab.com/WPezPlugins/wpez-widgets-demo/blob/master/App/Plugin/Container/Contents/ClassFormElements.php

### TODO

- TBD


### CHANGE LOG


- v0.0.1 - 4 April 2019
   
   Hey! Ho!! Let's go!!! 
   