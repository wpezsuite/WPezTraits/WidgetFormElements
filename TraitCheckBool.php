<?php

namespace WPez\WPezTraits\WidgetFormElements;

trait TraitCheckBool {

	public function widgetCheckBool( $arr_args = false ) {

		if ( ! is_array($arr_args) ){
			return false;
		}

		$arr_defaults =[
			'this' => false,
			'label' => false,
			'name' => false,
			'type' => 'false',
			'class' => 'checkbox',
			'value' => false,
		];

		$arr = array_merge($arr_defaults, $arr_args);

		if ( ! $arr['this'] instanceOf \WP_Widget) {
			return false;
		}

		if ( ! is_string($arr['name']) ){
			return false;
		}

		if ( empty($arr['name'] )){
			return false;
		}

		if ( $arr['type'] !== 'chk_bool'){
			return false;
		}

		if ( ! is_string($arr['class']) ){
			$arr['class'] = 'checkbox';
		}

		$str_ret = '';

		$str_ret .= '<input type="checkbox"';
		$str_ret .= ' id="'  . esc_attr( $arr['this']->get_field_id( $arr['name'] ) ) . '"';
		$str_ret .= ' class="' . esc_attr($arr['class']). '"';
		$str_ret .= ' ' . checked( $arr['value'], true, false  );
		$str_ret .= ' name="' . esc_attr( $arr['this']->get_field_name( $arr['name'] ) ) . '">';
		$str_ret .= ' <label for="' . esc_attr( $arr['this']->get_field_id( $arr['name'] ) ) . '"">';
		$str_ret .= esc_attr( $arr['label'] );
		$str_ret .= '</label>';

		echo $str_ret;


	}

}