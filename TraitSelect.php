<?php

namespace WPez\WPezTraits\WidgetFormElements;

trait TraitSelect{

	public function widgetSelect( $arr_args = false  ) {

		if ( ! is_array( $arr_args ) ) {
			return false;
		}

		$arr_defaults = [
			'this'    => false,
			'label'   => false,
			'name'    => false,
			'class'   => 'widefat',
			'type'    => false,
			'options' => false,
			'value'   => '',
		];

		$arr = array_merge( $arr_defaults, $arr_args );

		if ( ! $arr['this'] instanceOf \WP_Widget ) {
			return false;
		}

		if ( ! is_string( $arr['name'] ) ) {
			return false;
		}

		if ( empty( $arr['name'] ) ) {
			return false;
		}

		if ( $arr['type'] != 'select' ) {
			return false;
		}

		if ( ! is_string( $arr['class'] ) ) {
			$arr['class'] = 'widefat';
		}

		if ( ! is_array( $arr['options'] ) || empty( $arr['options'] ) ) {
			return false;
		}

		if ( is_string( $arr['label'] ) ) {
			echo '<label for="' . esc_attr( $arr['this']->get_field_id( $arr['name'] ) ) . '">';
			echo esc_attr( $arr['label'] );
			echo '</label> ';
		}

		$str_ret = '';

		$str_ret .= '<select';
		$str_ret .= ' id="' . esc_attr( $arr['this']->get_field_id( $arr['name'] ) ) . '"';
		$str_ret .= ' class="' . esc_attr( $arr['class'] ) . '"';
		$str_ret .= ' name="' . esc_attr( $arr['this']->get_field_name( $arr['name'] ) ) . '"';
		$str_ret .= '>';

		// TODO - only do select if this yields something. in theory it might not
		foreach ( $arr['options'] as $value => $display ) {

			if ( ! is_string($value) || ! is_string($display) ){
				continue;
			}

			$str_ret .= '<option value="' . esc_attr( $value ) . '" ' . selected( $arr['value' ], $value, false ) . '>' . esc_attr( $display ) . '</option>';
		}

		$str_ret .= '</select>';

		echo $str_ret;
	}

}