<?php

namespace WPez\WPezTraits\WidgetFormElements;

trait TraitCheckMulti {

	public function widgetCheckMulti( $arr_args = false ) {

		if ( ! is_array($arr_args) ){
			return false;
		}

		$arr_defaults =[
			'this' => false,
			'label' => false,
			'name' => false,
			'type' => 'false',
			'br'    => true,
			'class' => 'checkbox',
			'options' => false,
			'value' => [],
		];

		$arr = array_merge($arr_defaults, $arr_args);

		if ( ! $arr['this'] instanceOf \WP_Widget) {
			return false;
		}

		if ( ! is_string($arr['name']) ){
			return false;
		}

		if ( empty($arr['name'] )){
			return false;
		}

		if ( $arr['type'] !== 'chk_multi'){
			return false;
		}

		if ( ! is_string($arr['class']) ){
			$arr['class'] = 'checkbox';
		}

		if ( ! is_array( $arr['options'] ) || empty( $arr['options'] ) ) {
			return false;
		}

		$str_ret = '';
		$str_fieldset_close = '';
		if ( is_string( $arr['label'] ) ) {

			$str_ret .= '<fieldset>';
			$str_fieldset_close = '</fieldset>';
			$str_ret .= '<legend  style="margin-bottom:5px">';
			$str_ret .= esc_attr( $arr['label'] );
			$str_ret .= '</legend> ';
		}
		$str_id_slug =  $arr['this']->get_field_id( $arr['name']);

		$ndx = 0;
		foreach ( $arr['options'] as $value => $display ) {

			if ( ! is_string( $value ) || ! is_string( $display ) ) {
				continue;
			}

			$str_ret .= '<input type="checkbox"';
			$str_ret .= ' id="' . esc_attr($str_id_slug) . '-' . esc_attr($ndx) . '"';
			$str_ret .= ' name="' . esc_attr( $arr['this']->get_field_name( $arr['name'] ) ) . '[]"';
			$str_ret .= ' value="' . esc_attr( $value ) . '"';
			if ( in_array($value, $arr['value']) ) {
				$str_ret .= ' checked';
			}
			$str_ret .= '>';
			$str_ret .= '<label for="' . esc_attr($str_id_slug) . '-' . esc_attr($ndx) . '">' . esc_attr( $display ) . '</label>';
			if ( $arr['br'] === false ) {
				$str_ret .= ' &nbsp; ';
			} else {
				$str_ret .= '<br>';
			}
			$ndx++;
		}
		$str_ret .= $str_fieldset_close;
		echo $str_ret;


	}

}