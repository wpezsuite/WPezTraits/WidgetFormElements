<?php

namespace WPez\WPezTraits\WidgetFormElements;

trait TraitInput{

	public function widgetInput( $arr_args = false  ){

		if ( ! is_array($arr_args) ){
			return false;
		}

		$arr_defaults =[
			'this' => false,
			'label' => false,
			'name' => false,
			'class' => 'widefat',
			'type' => false,
			'value' => '',
			'placeholder' => false,
		];

		$arr = array_merge($arr_defaults, $arr_args);

		if ( ! $arr['this'] instanceOf \WP_Widget) {
			return false;
		}

		if ( ! is_string($arr['name']) ){
			return false;
		}

		if ( empty($arr['name'] )){
			return false;
		}

		if ( ! in_array($arr['type'], ['text', 'number' ]) ){
			return false;
		}

		if ( ! is_string($arr['class']) ){
			$arr['class'] = 'widefat';
		}

		if ( ! is_string($arr['placeholder']) ){
			$arr['placeholder'] = '';
		}

		if ( is_string( $arr['label'] ) ) {
			echo '<label for="' . esc_attr( $arr['this']->get_field_id( $arr['name'] ) ) . '">';
			echo esc_attr( $arr['label'] );
			echo '</label> ';
		}

		$str_ret = '';

		$str_ret .= '<input type="' . esc_attr($arr['type']) . '"';
		$str_ret .= ' id="'  . esc_attr( $arr['this']->get_field_id( $arr['name'] ) ) . '"';
		$str_ret .= ' class="' . esc_attr( $arr['class']) . '"';
		$str_ret .= ' name="' . esc_attr( $arr['this']->get_field_name( $arr['name'] ) ) . '"';
		$str_ret .= ' placeholder="' . esc_attr($arr['placeholder']) . '"';

		if ( $arr['type'] == 'number'){

			// https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/number
			$arr_defaults_num = [
				'min' => false,
				'max' => false,
				'step' => '1',
				'size' => '3'
			];

			$arr = array_merge($arr_defaults_num, $arr);

			if ( $arr['min'] !== false  && $arr['max'] !== false
			&& (float)$arr['min'] <= (float)$arr['max']) {
				$str_ret .= ' min="' . (float) $arr['min'] . '"';
				$str_ret .= ' max="' . (float) $arr['max'] . '"';
			} else {

				if ( $arr['min'] !== false ) {
					$str_ret .= ' min="' . (float) $arr['min'] . '"';
				}

				if ( $arr['max'] !== false ) {
					$str_ret .= ' max="' . (float) $arr['max'] . '"';
				}
			}
			if ( $arr['step'] !== false ) {
				$str_ret .= ' step="' . (float) $arr['step'] . '"';
			}

		}
		if ( isset($arr['size']) && $arr['size'] !== false ) {
			$str_ret .= ' size="' . (integer) $arr['size'] . '"';
		}
		$str_ret .= ' value="' . esc_attr( $arr['value'] );
		$str_ret .= '">';

		echo $str_ret;

	}
}
