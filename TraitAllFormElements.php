<?php

namespace WPez\WPezTraits\WidgetFormElements;

trait TraitAllFormElements {

	use \WPez\WPezTraits\WidgetFormElements\TraitCheckBool;
	use \WPez\WPezTraits\WidgetFormElements\TraitCheckMulti;
	use \WPez\WPezTraits\WidgetFormElements\TraitInput;
	use \WPez\WPezTraits\WidgetFormElements\TraitRadio;
	use \WPez\WPezTraits\WidgetFormElements\TraitSelect;
	use \WPez\WPezTraits\WidgetFormElements\TraitTextarea;

}